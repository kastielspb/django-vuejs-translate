from django.urls import path

from .views import JavaScriptCatalogView

app_name = 'vuejs_translate'

urlpatterns = [
     path('i18n.js', JavaScriptCatalogView.as_view(), name='js-i18n'),
]
